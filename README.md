# flowIO
Flow Cytometry Standard conformance testing, editing and export tool.   

### Installation  

```r
# install.packages("devtools")
devtools::install_bitbucket("Mira_Koblizek/flowIO")
```

### Usage

To launch interactive flowIO GUI (shiny app), run:

```r
library(flowIO)  
runFlowIOapp()
```

Alternatively run tests from R by running `chckAllFCS(full_path_to_fcs_files_to_be_checked)` for table of all groups of checks.   

  
### FCS sections being tested for compliance

To test for FCS standard complinace we used [FCS 3.1 Normative Reference](http://isac-net.org/Resources/Standards/FCS3-1.aspx).  
  
Not all paragraphs from the Normative Reference could be tested against. flowiO covers over 85 percent of the reference text. Remaining paragraphs were from chapter 2 (Terminology and General Requirements) including Conventions, Definitions and General Concepts sections as they were mainly too general, were a prerequisite for other checks or could not be checked for in principle (e.g. some definitions).  

flowIO also does not test against sections 3.4 ANALYSIS segment, 3.5 CRC Value and 3.6 Other Segments.  
  
  
The full list of Normative Reference paragraphs with information if they are covered by flowIO follows:  

2.1 no - [Conventions not testable]  
  
2.2.  [most Definitions not testable some exceptions below]  
  - 1 no  
  - 2 no  
  - 3 yes - see chckOffsets()[10:11,] [partial]  
  - 4 yes - partially tested by chckHeader() and chckOffsets()[14:15,]
  - 5 yes - see chckKeywordsDuplicity()  
  - 6 yes - partially tested by chckRequiredKeywords()  
  - 7 no  
  - 8 no  
  - 9 no  
  - 10 no  
  - 11 yes - see chckOffsets()[C(9, 13),]  
  - 12 no  
  - 13 no  
  - 14 no  
  - 15 yes - partially tested by getKeywords[[4]] and chckOffsets()[14,], which are both testing encoding of delimiter
  
2.3 no - [General Concepts not testable]    
  
3.1.  
  - 1 yes - see chckHeader(), chckOffsets()[c(1, 2, 3, 5, 7),]   
  
3.2.  
  - 1 yes - see chckEmptyKeywords()  [partial, test if keyword-values are in pairs (i.e. not empty)]      
  - 2 yes - see chckRequiredKeywords() for testing presence of all required keywords; see chckOffsets()[c(1, 14),] for testing of TEXT segment size  
  - 3 yes - see chckOffsets()[8]  [partial, content of supplemental TEXT not tested]  
  - 4 yes - see chckRequiredKeywords() and chckNumValues()   
  - 5 yes - see getKeywords() - it set the first character as delimiter  
  - 6 yes - see getKeywords()   
  - 7 yes - see getKeywords()[4]  [partial, double delimitors in keywords or values not testable]   
  - 8 yes - see getKeywords()[5]  [partial, keyword tested for ASCII 32-126]  
  - 9 yes - see getKeywords()[2]  
  - 10 yes - see chckKeywordsDuplicity()   
  - 11 no  - not testable  
  - 12 yes - see chckDollarKeywords()   
  - 13 no  - not testable  
  - 14 yes - see chckRequiredKeywords()   
  - 15 yes - see getKeywords()[[5]] This is only other definition of the same fact as in the beginning of paragraph 3.2.8  
  - 16 yes - see chckDollarKeywords() - if there is some numbered keyword with n > $PAR or n < 1, it is reported, chckRequiredKeywords() - if there is some numbered required keyword missing, it is reported  
  - 17 yes - see chckNumValues()   
  - 18 yes - see chckRequiredKeywords(), chckOffsets()[15]    
  - 19 yes - see chckDollarKeywords()   
  - 20 yes - see chckIndividualKeywords(), (chckOffsets()[4] for $BEGINDATA and $ENDDATA, chckOffsets()[8] for $BEGINSTEXT and $ENDSTEXT)   
   
3.3 no - [DATA extent tested elswere]   
  
3.4 no - [ANALYSIS is not generaly used]   

3.5 yes - see chckOffsets()[12]  [partial, just presence of 8 bytes]   

3.6 no - [Other Segments do not have enough definitions to be tested]  
