---
output:
  rmarkdown::html_vignette:
    fig_caption: yes
vignette: >
  %\VignetteIndexEntry{Vignette Title}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---
```{r setup, include=FALSE}
knitr::opts_chunk$set(cache=FALSE, echo = FALSE, message=FALSE, warning=FALSE)

require(knitr)
require(kableExtra)
require(xtable)
```


```{r supp_tab_all_checks_flowrepo_downloads, eval=FALSE, include=FALSE, results='asis'}
# ## Download and manual clena data from FlowRepository
# # # Do only once:
# # library(FlowRepositoryR)
# # ds <- flowRep.get("FR-FCM-ZZZ4")
# # ds <- download(ds, dirpath = "FR-FCM-ZZZ4/")
# 
# # fcs_files <- list.files("FR-FCM-ZZZ4", recursive = TRUE, full.names = TRUE, pattern = "\\.fcs$|\\.lmd$", ignore.case = TRUE)
# 
# # # dput(list.files("FR-FCM-ZZZ4", recursive = TRUE, full.names = TRUE)) # file after cleaning are:
# # fcs_files <- c("FR-FCM-ZZZ4/1 WT_001.fcs", "FR-FCM-ZZZ4/3215apc 100004.fcs",
# # "FR-FCM-ZZZ4/Accuri - C6 - A01 H2O.fcs", "FR-FCM-ZZZ4/Applied Biosystems - Attune.fcs",
# # "FR-FCM-ZZZ4/BD - FACS Aria II.fcs", "FR-FCM-ZZZ4/Beckman Coulter - Cyan.fcs",
# # "FR-FCM-ZZZ4/Beckman Coulter - Cytomics FC500.LMD", "FR-FCM-ZZZ4/Beckman Coulter - Gallios.LMD",
# # "FR-FCM-ZZZ4/Beckman Coulter - MoFlo Astrios - linear settings.fcs",
# # "FR-FCM-ZZZ4/Beckman Coulter - MoFlo Astrios - log settings.fcs",
# # "FR-FCM-ZZZ4/Beckman Coulter - MoFlo XDP.fcs", "FR-FCM-ZZZ4/Cytek DxP10 - 6-peak Q&b 11-06-2012 001.fcs",
# # "FR-FCM-ZZZ4/iCyt - Eclipse - 8 peak.lmd", "FR-FCM-ZZZ4/iCyt - Eclipse.lmd",
# # "FR-FCM-ZZZ4/Millipore - easyCyte 6HT-2L - InCyte.fcs", "FR-FCM-ZZZ4/Miltenyi Biotec - MACSQuant Analyzer.fcs",
# # "FR-FCM-ZZZ4/MVa2011-06-30_fcs31.fcs", "FR-FCM-ZZZ4/Partec - PAS - 8 peak beads.FCS",
# # "FR-FCM-ZZZ4/Stratedigm - S1400 - 8 Peaks Beads.fcs", "FR-FCM-ZZZ4/System II listmode with extra info in bits D10-D15.LMD",
# # "FR-FCM-ZZZ4/Verity Software House - GemStoneGeneratedData - 500000events.fcs"
# # )
# 
# 
# 
# 
# # c(1:2, 5:8, 11:14, 16:21) # will parse
# # c(3, 4, 15) # will not parse (Parsing status: There is something wrong with fcs_text in this part:  )
# # c(9, 10) # Error in keys[[match(i, toupper(names(keys)))]] : subscript out of bounds
# 
# 
# 
# chcks_l <- chckAllFCS(fcs_files[c(1:2, 5:8, 11:14, 16:21)], pass_msg = "·", fail_msg = "F")
# chcks_tbl <- do.call(cbind, lapply(chcks_l, `[`, 2))
# # colnames(chcks_tbl) <- sapply(strsplit(fcs_files[c(1:2, 5:8, 11:14, 16:21)], "/"), "[[", 2)
# colnames(chcks_tbl) <- paste0("fcs_", c(1:2, 5:8, 11:14, 16:21))
# chcks_tbl <- data.frame(test = chcks_l[[1]][, 1], chcks_tbl)
# 
# # dput(chcks_tbl)
# # structure(list(test = structure(c(7L, 2L, 1L, 6L, 8L, 9L, 4L, 
# # 5L, 3L), .Label = c("Allowed characters check", "Byte offsets check", 
# # "Check individual keywords compliance", "Check of non-empty values", 
# # "Check of numeric format", "Duplicity check", "Header format check", 
# # "Required keywords check", "Standard FCS keywords check"), class = "factor"), 
# #     fcs_1 = c("F", "F", "·", "·", "·", "·", "·", "F", "F"
# #     ), fcs_2 = c("·", "F", "·", "·", "·", "·", "·", "·", 
# #     "·"), fcs_5 = c("F", "F", "·", "·", "·", "F", "·", "·", 
# #     "F"), fcs_6 = c("·", "F", "·", "·", "·", "·", "·", 
# #     "·", "F"), fcs_7 = c("·", "F", "·", "·", "F", "F", "·", 
# #     "F", "F"), fcs_8 = c("·", "F", "·", "·", "F", "F", "·", 
# #     "F", "F"), fcs_11 = c("·", "F", "·", "·", "·", "·", 
# #     "·", "·", "F"), fcs_12 = c("·", "F", "·", "·", "·", 
# #     "·", "·", "·", "·"), fcs_13 = c("·", "F", "·", "·", 
# #     "·", "F", "·", "·", "F"), fcs_14 = c("·", "F", "·", 
# #     "·", "·", "F", "·", "·", "F"), fcs_16 = c("·", "F", 
# #     "·", "·", "·", "·", "·", "·", "F"), fcs_17 = c("·", 
# #     "F", "·", "·", "·", "·", "·", "F", "F"), fcs_18 = c("·", 
# #     "F", "·", "·", "F", "F", "·", "F", "F"), fcs_19 = c("·", 
# #     "·", "·", "·", "·", "·", "·", "·", "F"), fcs_20 = c("·", 
# #     "F", "·", "·", "F", "F", "·", "F", "F"), fcs_21 = c("·", 
# #     "F", "·", "·", "·", "·", "·", "·", "F")), .Names = c("test", 
# # "fcs_1", "fcs_2", "fcs_5", "fcs_6", "fcs_7", "fcs_8", "fcs_11", 
# # "fcs_12", "fcs_13", "fcs_14", "fcs_16", "fcs_17", "fcs_18", "fcs_19", 
# # "fcs_20", "fcs_21"), row.names = c(NA, -9L), class = "data.frame")
# 
# 
# print(xtable(chcks_tbl, caption = "**Table tab_all_checks** · - passed tests, F - failed tests."), type="html", include.rownames=FALSE)
```


```{r supp_tab_all_checks_flowrepo_offline_prepare, results='asis'}
fcs_files <- c("FR-FCM-ZZZ4/1 WT_001.fcs", "FR-FCM-ZZZ4/3215apc 100004.fcs",
"FR-FCM-ZZZ4/Accuri - C6 - A01 H2O.fcs", "FR-FCM-ZZZ4/Applied Biosystems - Attune.fcs",
"FR-FCM-ZZZ4/BD - FACS Aria II.fcs", "FR-FCM-ZZZ4/Beckman Coulter - Cyan.fcs",
"FR-FCM-ZZZ4/Beckman Coulter - Cytomics FC500.LMD", "FR-FCM-ZZZ4/Beckman Coulter - Gallios.LMD",
"FR-FCM-ZZZ4/Beckman Coulter - MoFlo Astrios - linear settings.fcs",
"FR-FCM-ZZZ4/Beckman Coulter - MoFlo Astrios - log settings.fcs",
"FR-FCM-ZZZ4/Beckman Coulter - MoFlo XDP.fcs", "FR-FCM-ZZZ4/Cytek DxP10 - 6-peak Q&b 11-06-2012 001.fcs",
"FR-FCM-ZZZ4/iCyt - Eclipse - 8 peak.lmd", "FR-FCM-ZZZ4/iCyt - Eclipse.lmd",
"FR-FCM-ZZZ4/Millipore - easyCyte 6HT-2L - InCyte.fcs", "FR-FCM-ZZZ4/Miltenyi Biotec - MACSQuant Analyzer.fcs",
"FR-FCM-ZZZ4/MVa2011-06-30_fcs31.fcs", "FR-FCM-ZZZ4/Partec - PAS - 8 peak beads.FCS",
"FR-FCM-ZZZ4/Stratedigm - S1400 - 8 Peaks Beads.fcs", "FR-FCM-ZZZ4/System II listmode with extra info in bits D10-D15.LMD",
"FR-FCM-ZZZ4/Verity Software House - GemStoneGeneratedData - 500000events.fcs"
)

chcks_tbl <- structure(list(test = structure(c(7L, 2L, 1L, 6L, 8L, 9L, 4L, 
5L, 3L), .Label = c("Allowed characters check", "Byte offsets check", 
"Check individual keywords compliance", "Check of non-empty values", 
"Check of numeric format", "Duplicity check", "Header format check", 
"Required keywords check", "Standard FCS keywords check"), class = "factor"), 
    fcs_1 = c("F", "F", "·", "·", "·", "·", "·", "F", "F"
    ), fcs_2 = c("·", "F", "·", "·", "·", "·", "·", "·", 
    "·"), fcs_5 = c("F", "F", "·", "·", "·", "F", "·", "·", 
    "F"), fcs_6 = c("·", "F", "·", "·", "·", "·", "·", 
    "·", "F"), fcs_7 = c("·", "F", "·", "·", "F", "F", "·", 
    "F", "F"), fcs_8 = c("·", "F", "·", "·", "F", "F", "·", 
    "F", "F"), fcs_11 = c("·", "F", "·", "·", "·", "·", 
    "·", "·", "F"), fcs_12 = c("·", "F", "·", "·", "·", 
    "·", "·", "·", "·"), fcs_13 = c("·", "F", "·", "·", 
    "·", "F", "·", "·", "F"), fcs_14 = c("·", "F", "·", 
    "·", "·", "F", "·", "·", "F"), fcs_16 = c("·", "F", 
    "·", "·", "·", "·", "·", "·", "F"), fcs_17 = c("·", 
    "F", "·", "·", "·", "·", "·", "F", "F"), fcs_18 = c("·", 
    "F", "·", "·", "F", "F", "·", "F", "F"), fcs_19 = c("·", 
    "·", "·", "·", "·", "·", "·", "·", "F"), fcs_20 = c("·", 
    "F", "·", "·", "F", "F", "·", "F", "F"), fcs_21 = c("·", 
    "F", "·", "·", "·", "·", "·", "·", "F")), .Names = c("test", 
"fcs_1", "fcs_2", "fcs_5", "fcs_6", "fcs_7", "fcs_8", "fcs_11", 
"fcs_12", "fcs_13", "fcs_14", "fcs_16", "fcs_17", "fcs_18", "fcs_19", 
"fcs_20", "fcs_21"), row.names = c(NA, -9L), class = "data.frame")
```


```{r supp_tab_all_checks_flowrepo__2,fig.wide = TRUE, results='asis'}
kable_styling(
  kable(chcks_tbl, format = "html", row.names = FALSE),
  font_size = 10
  )
```


`r paste0("**Supplementary Table 2** Results of compliance test of files from FR-FCM-ZZZ4 FlowRepository collection. Tested files are: ", 
                              paste0(paste0("fcs_", c(1:2, 5:8, 11:14, 16:21)), " - ", sapply(strsplit(fcs_files[c(1:2, 5:8, 11:14, 16:21)], "/"), "[[", 2), collapse = ", "), " Test results are · - passed, F - failed test.")`   

